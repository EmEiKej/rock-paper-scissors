using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Bot : MonoBehaviour
{
    [SerializeField] List<Sprite> sprites;
    public int point = 0;
    public void DrawBotSprite() 
    {
        int random = Random.Range(0, sprites.Count);
        this.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = sprites[random];
    }
    public void ResetBotSprite() 
    {
        this.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
    }
    public void AddPointToBot() 
    {
        this.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = point.ToString();
    }
}
